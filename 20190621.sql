﻿USE ciclistas;

/*** Proyeccion ***/
SELECT DISTINCT c.nombre FROM ciclista c;

SELECT DISTINCT nombre FROM ciclista;

/*** Combino ambos operadores ***/
SELECT DISTINCT
  c.nombre
FROM
  ciclista c 
WHERE 
  c.edad<30;

-- Indicame las edades de los ciclistas de banesto
SELECT DISTINCT edad FROM ciclista c WHERE c.nomequipo='Banesto';


-- Indicame los nombre de los equipos de los ciclistas que tienen menos de 30 años
SELECT DISTINCT 
  c.nomequipo 
FROM 
  ciclista c 
WHERE 
  c.edad<30;


/**
  and y or
**/

-- Listar los nombres de los ciclistas 
-- cuya edad esta entre 30 y 32 (inclusive)
SELECT 
  DISTINCT c.nombre 
FROM 
  ciclista c 
WHERE
  c.edad>=30 AND c.edad<=32;

-- modificar con los operadores extendidos
SELECT 
  DISTINCT c.nombre 
FROM 
  ciclista c 
WHERE 
  c.edad BETWEEN 30 AND 32;

-- Listar los equipos que tengan ciclistas
-- menores de 31 años y que su nombre comience por M
SELECT DISTINCT c.nomequipo FROM ciclista c WHERE c.edad<31 AND c.nombre LIKE 'M%';

-- Listar los equipos que tengan ciclistas
-- menores de 31 años o que su nombre comience por M
SELECT DISTINCT c.nomequipo FROM ciclista c WHERE c.edad<31 OR c.nombre LIKE 'M%';

-- Listar los ciclistas de banesto y de kelme
SELECT c.dorsal,
       c.nombre,
       c.edad,
       c.nomequipo
FROM 
  ciclista c
WHERE 
  c.nomequipo IN ('banesto','kelme');


























