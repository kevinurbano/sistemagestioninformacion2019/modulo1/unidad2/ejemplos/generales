﻿USE teoria1;

/*
  1.- Indicar el nombre de las marcas que se hayam alquilado coches
*/

  -- Crear la vista
  CREATE OR REPLACE VIEW consulta1 AS
    SELECT 
      DISTINCT c.marca
      FROM 
        coches c    
      JOIN
        alquileres a ON c.codigoCoche = a.coche;

  -- Ejecutar la vista
  SELECT * FROM consulta1 c;

  /* 
    Vamos a realizar la misma pero optimizada 
  */

  -- c1Consulta1: coches alquilados.
  CREATE OR REPLACE VIEW c1Consulta1 AS
    SELECT 
      DISTINCT a.coche
      FROM
        alquileres a;

  -- Consulta final
  CREATE OR REPLACE VIEW consulta1 AS
    SELECT 
      DISTINCT marca
      FROM 
        c1Consulta1 c1
      JOIN
        coches c ON c1.coche=c.codigoCoche;

/* 
  2.- Nombre de los ususarios que hayan algquilado alguna vez coches 
*/
  -- Sin optimizar
  CREATE OR REPLACE VIEW consulta2 AS 
    SELECT DISTINCT u.nombre 
      FROM 
        usuarios u 
      JOIN 
        alquileres a ON u.codigoUsuario = a.usuario;

  -- Optimizada
    -- c1Consulta2 : Los usuarios que han alquilado coches.
    CREATE OR REPLACE VIEW c1Consulta2 AS 
      SELECT DISTINCT a.usuario FROM alquileres a;
    
    -- Consulta final
    CREATE OR REPLACE VIEW consulta2 AS
      SELECT DISTINCT u.nombre FROM c1Consulta2 c
        JOIN usuarios u ON c.usuario= u.codigoUsuario;

  -- Sin vista
    -- C1
    SELECT 
      DISTINCT a.usuario
      FROM 
        alquileres a;

    -- Final
    SELECT DISTINCT u.nombre 
      FROM 
        (
          SELECT 
            DISTINCT a.usuario 
            FROM 
              alquileres a 
        ) c1
      JOIN 
        usuarios u ON c1.usuario=u.codigoUsuario;     

/*
  3.- coches que no han sido alquilados  
*/
  -- c1Consulta3
  CREATE OR REPLACE VIEW c1Consulta3 AS
    SELECT 
      DISTINCT a.coche 
      FROM 
        alquileres a;
  
  -- Final
  CREATE OR REPLACE VIEW consulta3 AS
    SELECT 
      DISTINCT c.marca 
      FROM 
        coches c 
      LEFT JOIN
        c1consulta3 c1 ON c.codigoCoche=c1.coche 
      WHERE 
        c1.coche IS NULL;



