﻿USE teoria1;

/*
  1.- Indicar el nombre de las marcas que se hayam alquilado coches
*/

  SELECT 
    DISTINCT c.marca
    FROM 
      coches c    
    JOIN
      alquileres a ON c.codigoCoche = a.coche;
  
  -- C1
  SELECT 
    DISTINCT a.coche
    FROM
      alquileres a;

  -- Final
  SELECT 
    DISTINCT marca
    FROM 
      (
      SELECT 
        DISTINCT a.coche
        FROM
          alquileres a
      ) c1
    JOIN
      coches c ON c1.coche=c.codigoCoche;
