﻿USE ciclistas;

/*
  Cuantas etapas ha ganado cada ciclista 
  dorsal,numeroEtapas
  1,3
  2,4
*/
SELECT e.dorsal,COUNT(*) numEtapas 
  FROM etapa e 
  GROUP BY e.dorsal;

/*
  nombre de los ciclistas han ganado mas de 2 etapas  
*/
  SELECT c.nombre FROM (
      SELECT e.dorsal 
        FROM etapa e         
        GROUP BY e.dorsal 
        HAVING COUNT(*)>2
  ) c1 JOIN ciclista c USING(dorsal);


/*
  El nombre del ciclista que tiene mas edad  
*/
  -- C1 : edad maxima
  SELECT MAX(c.edad) maxima FROM ciclista c;

  -- Solucion con Join
  SELECT c.nombre
    FROM ciclista c 
    JOIN (SELECT MAX(c.edad) maxima FROM ciclista c) c1 
    ON c.edad=c1.maxima;

  -- Solucion con Where
  SELECT c.nombre 
    FROM ciclista c 
    WHERE c.edad=(SELECT MAX(c.edad) maxima FROM ciclista c);