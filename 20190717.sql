﻿USE teoria1;

-- 1.- Numero de coches que ha alquilado usuario 1
  -- Numero de alquileres
  SELECT 
    COUNT(*) nAlquileres
  FROM 
    alquileres a 
  WHERE 
    a.usuario=1;

  -- Numero de coches
  SELECT 
    COUNT(DISTINCT a.coche) nCoches
  FROM 
    alquileres a 
  WHERE 
    a.usuario=1;

-- 2.- Numero de alquileres por mes
  SELECT 
    MONTH(a.fecha) meses,COUNT(*) nAlquileres 
  FROM
    alquileres a 
  GROUP BY 
    MONTH(a.fecha);

-- 3.- Numero de usuarios por sexo
  SELECT 
    u.sexo, COUNT(*) nTotal 
  FROM 
    usuarios u 
  GROUP BY 
    u.sexo;

-- 4.- Numero de alquileres de coches por color
  SELECT c.color,COUNT(*) nAlquiler
    FROM 
      coches c
    JOIN 
      alquileres a
    ON c.codigoCoche = a.coche
    GROUP BY c.color;

-- 5.- Numero de marcas de coches 
  SELECT 
    COUNT(DISTINCT c.marca) nMarcas
  FROM 
    coches c;

-- 6.- Numero de marcas de coches alquilados  
  SELECT c.marca,COUNT(*) nAlquileres
    FROM 
      coches c 
    JOIN 
      alquileres a
    ON c.codigoCoche = a.coche
    GROUP BY c.marca;

  -- Otra interpretacion
  SELECT 
    COUNT(DISTINCT c.marca) nMarcas
  FROM 
    coches c 
  JOIN 
    alquileres a 
  ON c.codigoCoche = a.coche;

     