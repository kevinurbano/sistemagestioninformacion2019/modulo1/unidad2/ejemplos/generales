﻿USE paginaweb;

/*
  1.- Titulo del libro y nombre del autor que lo ha escrito.
*/
  -- Producto Cartesiano
  SELECT a.nombre_completo, l.titulo 
    FROM libro l, autor a
    WHERE l.autor=a.id_autor;
  
  -- Con JOIN
  SELECT a.nombre_completo,l.titulo 
    FROM libro l JOIN autor a
    ON l.autor = a.id_autor;

  -- Con JOIN como producto cartesiano (no utilizar)
  SELECT a.nombre_completo,l.titulo 
    FROM libro l JOIN autor a
    WHERE l.autor = a.id_autor;

/*
  2.- Nombre del autor y titulo del libro en el que han ayudado.
*/
  SELECT a1.nombre_completo,l.titulo
    FROM ayuda a 
    JOIN libro l ON a.libro = l.id_libro
    JOIN autor a1 ON a.autor = a1.id_autor;

/*
  3.- Los libros que se ha descargado cada
      usuario con la fecha de descarga. Listar el login,
      fecha descarga, id_libro 
*/
  SELECT * FROM fechadescarga f; 

/*
  4.- los libros que se ha decargado cada usuario con la fecha de descarga.
      Listar el login, correo, fecha descarga, titulo del libro.
*/
  SELECT f.usuario, u.email, f.fecha_descarga, l.titulo
    FROM fechadescarga f 
    JOIN descarga d ON f.libro = d.libro AND f.usuario = d.usuario
    JOIN usuario u ON d.usuario = u.login
    JOIN libro l ON d.libro=l.id_libro;
  
  SELECT f.usuario, u.email, f.fecha_descarga, l.titulo
    FROM fechadescarga f 
    JOIN descarga d JOIN usuario u JOIN libro l
    ON f.libro = d.libro 
    AND f.usuario = d.usuario
    AND d.usuario = u.login
    AND d.libro=l.id_libro;

/*
  5.- El numero de libros que hay  
*/
  SELECT COUNT(*) nlibros 
    FROM libro l;

/* 
  6.- El numero de libros por coleccion
      (no hace falta colocar nombre de la coleccion) 
*/
  SELECT l.coleccion,COUNT(*) nlibros 
    FROM libro l 
    GROUP BY l.coleccion;

/*
  7.- La coleccion que tiene mas libros 
      (no hace falta colocar nombre de la coleccion)
*/

  -- Subconsulta C1
  SELECT l.coleccion,COUNT(*) nlibros 
    FROM libro l 
    GROUP BY l.coleccion;

  -- Subconsulta C2
  SELECT MAX(c2.nlibros) maximo FROM 
    (SELECT l.coleccion,COUNT(*) nlibros 
      FROM libro l 
      GROUP BY l.coleccion) c2;

  -- Final
  SELECT c1.coleccion
    FROM (
      SELECT l.coleccion,COUNT(*) nlibros 
        FROM libro l 
        GROUP BY l.coleccion
    ) c1
    JOIN (
      SELECT MAX(c1.nlibros) maximo FROM 
        (SELECT l.coleccion,COUNT(*) nlibros 
          FROM libro l 
          GROUP BY l.coleccion) c1
    ) c2
    ON c1.nlibros=c2.maximo;

  -- Final con WHERE
  SELECT c1.coleccion
    FROM (
      SELECT l.coleccion,COUNT(*) nlibros 
        FROM libro l 
        GROUP BY l.coleccion
    ) c1
    WHERE c1.nlibros=(
        SELECT MAX(c2.nlibros) maximo FROM 
          (SELECT l.coleccion,COUNT(*) nlibros 
            FROM libro l 
            GROUP BY l.coleccion) c2
      );

  -- Final con HAVING
  SELECT l.coleccion,COUNT(*) nlibros 
    FROM libro l 
    GROUP BY l.coleccion
    HAVING nlibros=(
        SELECT MAX(c2.nlibros) maximo FROM 
          (SELECT l.coleccion,COUNT(*) nlibros 
            FROM libro l 
            GROUP BY l.coleccion) c2
      );

/*
  8.- La coleccion que tiene menos libros
      (no hace falta colocar nombre de l a coleccion)  
*/

  -- Subconsulta C1
  SELECT l.coleccion,COUNT(*) nlibros 
    FROM libro l 
    GROUP BY l.coleccion;

  -- Subconsulta C2
  SELECT MIN(c2.nlibros) maximo FROM 
    (SELECT l.coleccion,COUNT(*) nlibros 
      FROM libro l 
      GROUP BY l.coleccion) c2;

  -- Final
  SELECT c1.coleccion
    FROM (
      SELECT l.coleccion,COUNT(*) nlibros 
        FROM libro l 
        GROUP BY l.coleccion
    ) c1
    JOIN (
      SELECT MIN(c1.nlibros) minimo FROM 
        (SELECT l.coleccion,COUNT(*) nlibros 
          FROM libro l 
          GROUP BY l.coleccion) c1
    ) c2
    ON c1.nlibros=c2.minimo;

/*
  9.- El nombre de la coleccion que tiene mas libros  
*/
  SELECT c.nombre FROM coleccion c
    JOIN (
      SELECT c1.coleccion
        FROM (
          SELECT l.coleccion,COUNT(*) nlibros 
            FROM libro l 
            GROUP BY l.coleccion
        ) c1
        JOIN (
          SELECT MAX(c1.nlibros) maximo FROM 
            (SELECT l.coleccion,COUNT(*) nlibros 
              FROM libro l 
              GROUP BY l.coleccion) c1
        ) c2
        ON c1.nlibros=c2.maximo
    ) consulta7
    ON c.id_coleccion=consulta7.coleccion;

/*
  10.- El nombre de la coleccion que tiene menos libros
*/
  SELECT c.nombre
    FROM (
      SELECT l.coleccion,COUNT(*) nlibros 
        FROM libro l 
        GROUP BY l.coleccion
      ) c1
      JOIN (
        SELECT MIN(c1.nlibros) minimo FROM 
          (SELECT l.coleccion,COUNT(*) nlibros 
            FROM libro l 
            GROUP BY l.coleccion) c1
    ) c2
    ON c1.nlibros=c2.minimo
    JOIN 
      coleccion c
    ON c1.coleccion=c.id_coleccion;
  
/* 
  11.- El nombre del libro que se ha descargado mas veces 
*/

  -- C1 
  -- El numero de descargas por cada libro
  SELECT 
    libro,COUNT(*) ndescargas 
    FROM 
      fechadescarga f
    GROUP BY 
      f.libro;

  -- C2
  -- El número maximo de descargas por libro
  SELECT 
    MAX(c1.ndescargas) maximo 
    FROM (
      SELECT 
        libro,COUNT(*) ndescargas 
      FROM 
        fechadescarga f
      GROUP BY 
        f.libro
    ) c1;

  -- C3
  -- Codigo del libro que se ha descargado mas veces
  SELECT c1.libro
    FROM (
        SELECT 
          libro,COUNT(*) ndescargas 
          FROM 
            fechadescarga f
          GROUP BY 
            f.libro
      ) c1
    JOIN (
      SELECT 
        MAX(c1.ndescargas) maximo 
        FROM (
          SELECT 
            libro,COUNT(*) ndescargas 
          FROM 
            fechadescarga f
          GROUP BY 
            f.libro
        ) c1
    ) c2
    ON c1.ndescargas=c2.maximo;

    -- Final
    SELECT l.titulo FROM libro l
      JOIN (
         SELECT c1.libro
          FROM (
              SELECT 
                libro,COUNT(*) ndescargas 
                FROM 
                  fechadescarga f
                GROUP BY 
                  f.libro
            ) c1
          JOIN (
            SELECT 
              MAX(c1.ndescargas) maximo 
              FROM (
                SELECT 
                  libro,COUNT(*) ndescargas 
                FROM 
                  fechadescarga f
                GROUP BY 
                  f.libro
              ) c1
          ) c2
          ON c1.ndescargas=c2.maximo
      ) c3 ON l.id_libro=c3.libro;


/*
  12.- El nombre del usuario que ha descargado mas libros
*/
  -- C1
  -- Numero de descargas por usuario
  SELECT 
    f.usuario,COUNT(*) ndescargas 
  FROM 
    fechadescarga f 
  GROUP BY 
    f.usuario;

  -- C2
  -- Numero maximo de descargas por usuario
    SELECT MAX(c1.ndescargas) FROM (
         SELECT 
          f.usuario,COUNT(*) ndescargas 
          FROM 
            fechadescarga f 
          GROUP BY 
            f.usuario
      ) c1;

  -- Final con JOIN
    SELECT C1.usuario
      FROM (
        SELECT 
          f.usuario,COUNT(*) ndescargas 
        FROM 
          fechadescarga f 
        GROUP BY 
          f.usuario
      ) C1
      JOIN (
        SELECT MAX(c1.ndescargas) maximo FROM (
         SELECT 
          f.usuario,COUNT(*) ndescargas 
          FROM 
            fechadescarga f 
          GROUP BY 
            f.usuario
        ) c1
      ) C2
      ON C1.ndescargas=C2.maximo;

  -- Final con HAVING
  SELECT 
    f.usuario, COUNT(*) ndescargas
  FROM 
    fechadescarga f 
  GROUP BY 
    f.usuario
  HAVING
    ndescargas=(
      SELECT MAX(c1.ndescargas) FROM (
       SELECT 
        f.usuario,COUNT(*) ndescargas 
        FROM 
          fechadescarga f 
        GROUP BY 
          f.usuario
      ) c1
    );



/*
  13.- El nombre de los usuario que ha descargado mas libros que Adam3
*/
  SELECT usuario
    FROM 
      fechadescarga f 
    GROUP BY 
      f.usuario
    HAVING
      COUNT(*)=(
        SELECT COUNT(*) 
          FROM
            fechadescarga f 
          WHERE 
            f.usuario='Adam3'
        );

/*
  14.- El mes que mas libros se han descargado
*/

  -- C1
  SELECT 
    MONTH(f.fecha_descarga) meses,COUNT(*) ndescargas 
    FROM 
      fechadescarga f 
    GROUP BY 
      meses;

  -- C2 
  SELECT 
    MAX(C1.ndescargas) 
    FROM ( 
      SELECT 
        MONTH(f.fecha_descarga) meses,COUNT(*) ndescargas 
        FROM 
          fechadescarga f 
        GROUP BY 
          meses
    ) C1;

  -- Final
  SELECT 
    c1.meses
    FROM (
      SELECT 
        MONTH(f.fecha_descarga) meses,COUNT(*) ndescargas 
        FROM 
          fechadescarga f 
        GROUP BY 
          meses    
    ) c1
    JOIN (
      SELECT 
        MAX(C1.ndescargas) maximo 
        FROM ( 
          SELECT 
            MONTH(f.fecha_descarga) meses,COUNT(*) ndescargas 
            FROM 
              fechadescarga f 
            GROUP BY 
              meses
        ) C1
    ) c2
    ON c1.ndescargas=c2.maximo;
