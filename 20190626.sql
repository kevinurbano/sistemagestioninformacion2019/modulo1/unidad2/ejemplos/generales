﻿USE ciclistas;

/* Numero de etapas que hay */
SELECT COUNT(*) FROM etapa e;

/* Numero de maillots que hay */
SELECT COUNT(*) FROM maillot m;

/* Numero de ciclistas que hayan ganado alguna etapa */
-- Sin subconsulta
SELECT COUNT(DISTINCT e.dorsal) total FROM etapa e;

-- Con subconsulta
  -- c1: dorsal de los ciclistas que han ganado etapas
  SELECT DISTINCT e.dorsal FROM etapa e;

  -- final
  SELECT COUNT(*) total FROM (SELECT DISTINCT e.dorsal FROM etapa e) c1;
