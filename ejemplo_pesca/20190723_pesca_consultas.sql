﻿USE pesca;

-- 1.- Indica el nombre de los clubes que tienen pescadores.
  SELECT DISTINCT
    c.nombre 
    FROM 
      clubes c
    JOIN 
      pescadores p ON c.cif = p.club_cif;

-- 2.- Indica el nombre del club que no tiene pescadores.
  SELECT 
    c.nombre 
    FROM
      clubes c
    LEFT JOIN
      pescadores p ON c.cif = p.club_cif
    WHERE
      p.club_cif IS NULL;

-- 3.- Indica el nombre de los cotos autorizados a algun club.
  SELECT 
    DISTINCT a.coto_nombre 
    FROM
      autorizados a;

-- 4.- Indica la provincia que tiene cotos autorizados a algun club.
  SELECT 
    DISTINCT c.provincia
    FROM 
      autorizados a
    JOIN
      cotos c ON a.coto_nombre = c.nombre;

-- 5.- Indica el nombre de los cotos que no estan autorizados a ningun club.
  SELECT 
    DISTINCT c.nombre
    FROM 
      cotos c 
    LEFT JOIN 
      autorizados a ON c.nombre = a.coto_nombre 
    WHERE 
      a.coto_nombre IS NULL;

-- 6.- Indica el numero de rios por provincia con cotos
   SELECT 
    c.provincia, COUNT(DISTINCT c.rio) nRios 
    FROM 
      cotos c 
    GROUP BY 
      c.provincia;

-- 7.- Indica el numero de rios por provincia con cotos autorizados
  SELECT 
    c.provincia,COUNT(DISTINCT c.rio) nRios
    FROM 
      cotos c
    JOIN
      autorizados a ON c.nombre = a.coto_nombre
    GROUP BY c.provincia;

-- 8.- Indica el nombre de la provincia con mas cotos autorizados
  
  -- C1: Numero de cotos autorizados por provincia
  SELECT 
    c.provincia, COUNT(DISTINCT nombre) ncotos
    FROM 
      cotos c
    JOIN 
      autorizados a ON c.nombre = a.coto_nombre
    GROUP BY
      c.provincia;

  -- C2: numero maximo de cotos por provincia
  SELECT 
    MAX(c1.ncotos) maximo
    FROM (
      SELECT 
        c.provincia, COUNT(DISTINCT nombre) ncotos
        FROM 
          cotos c
        JOIN 
          autorizados a ON c.nombre = a.coto_nombre
        GROUP BY
          c.provincia
    ) c1 ;

  -- Final c1 join c2
  SELECT c1.provincia FROM 
    (
      SELECT 
        c.provincia, COUNT(DISTINCT nombre) ncotos
        FROM 
          cotos c
        JOIN 
          autorizados a ON c.nombre = a.coto_nombre
        GROUP BY
          c.provincia
    ) c1
    JOIN
    (
      SELECT 
        MAX(c1.ncotos) maximo
        FROM (
          SELECT 
            c.provincia, COUNT(DISTINCT nombre) ncotos
            FROM 
              cotos c
            JOIN 
              autorizados a ON c.nombre = a.coto_nombre
            GROUP BY
              c.provincia
        ) c1 
    ) c2
    ON c1.ncotos=c2.maximo;

  -- Final c1 having c2
  SELECT 
    c.provincia
    FROM 
      cotos c
    JOIN 
      autorizados a ON c.nombre = a.coto_nombre
    GROUP BY
      c.provincia
    HAVING
      COUNT(*)=(
        SELECT 
          MAX(c1.ncotos) maximo
          FROM (
            SELECT 
              c.provincia, COUNT(DISTINCT nombre) ncotos
              FROM 
                cotos c
              JOIN 
                autorizados a ON c.nombre = a.coto_nombre
              GROUP BY
                c.provincia
          ) c1
      );

-- 9.- Indica el nombre del pescador y el nombre de su ahijado.
  SELECT 
    p.nombre Padrino, p1.nombre Ahijado
    FROM 
      apadrinar a
    JOIN
      pescadores p ON a.padrino = p.numSocio
    JOIN 
      pescadores p1 ON a.ahijado = p.numSocio;

  
  SELECT 
    padrinos.nombre,ahijados.nombre
    FROM 
      pescadores padrinos 
    JOIN 
      apadrinar a ON padrinos.numSocio=a.padrino
    JOIN
      pescadores ahijados ON ahijados.numSocio=a.ahijado;

-- 10.- Indica el numero de ahijados de cada pescador.
  SELECT 
    a.padrino,COUNT(*) nAhijados 
    FROM 
      apadrinar a 
    GROUP BY 
      a.padrino;
/*
  Vista para no permitir que el padrino no sea el mismo ahijado 
  Al hacer el insert en la vista no deberia dejar 
*/
  CREATE OR REPLACE VIEW vista1 AS 
    SELECT a.padrino,a.ahijado FROM apadrinar a
    WHERE a.ahijado<>a.padrino
    WITH LOCAL CHECK OPTION;

  