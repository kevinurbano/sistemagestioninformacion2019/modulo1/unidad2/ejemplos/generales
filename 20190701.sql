﻿USE ciclistas;

/*
  Ciclistas que han ganado etapas
  Mostral dorsal
*/
  SELECT DISTINCT e.dorsal FROM etapa e;

/*
  Ciclistas que han ganado puertos
  Mostral dorsal
*/
  SELECT DISTINCT p.dorsal FROM puerto p;


/* Operacion de Conjuntos */

/*
  Dorsal de los ciclistas que han ganado
  etapas o puertos  
*/
  SELECT DISTINCT e.dorsal FROM etapa e 
    UNION
  SELECT DISTINCT p.dorsal FROM puerto p;

/*
  Dorsal de los ciclistas que han ganado
  etapas y puertos  
*/

  /*SELECT * FROM 
    (SELECT DISTINCT e.dorsal FROM etapa e) c1 
    NATURAL JOIN 
    (SELECT DISTINCT p.dorsal FROM puerto p) c2;*/

  SELECT c1.dorsalEtapa dorsal FROM 
    (SELECT DISTINCT e.dorsal dorsalEtapa FROM etapa e) c1
    JOIN
    (SELECT DISTINCT p.dorsal dorsalPuerto FROM puerto p) c2 
    ON dorsalEtapa=dorsalPuerto;


/*
  Listado de todas las etapas y 
  el ciclista que la ha ganado
*/
  SELECT * FROM ciclista c JOIN etapa e ON c.dorsal = e.dorsal;

/*
  Listado de todos los puertos y 
  el ciclista que las ha ganado
*/
  SELECT * FROM puerto p JOIN ciclista c ON p.dorsal = c.dorsal;

/*
  Combinaciones interna  
*/

-- Listar todos los ciclistas con todos los datos del equipo
-- al que pertenecen
  SELECT * 
    FROM equipo e JOIN ciclista c 
    ON e.nomequipo = c.nomequipo;

  SELECT * 
    FROM equipo e JOIN ciclista c 
    USING(nomequipo);


/*
  Producto Cartesiano
*/
  SELECT * 
    FROM ciclista c,equipo e;


-- Convierto el producto cartesiano en una combinacion interna
  SELECT * 
    FROM ciclista c,equipo e
    WHERE c.nomequipo=e.nomequipo;

/*
  Listarme los nombres del ciclista y del equipo de aquellos 
  ciclistas que hayan ganado puertos
*/
SELECT DISTINCT c.nombre,c.nomequipo FROM ciclista c JOIN puerto p ON c.dorsal = p.dorsal;

/*
  Listarme los nombres del ciclista y del equipo de aquellos 
  ciclistas que hayan ganado etapa
*/
SELECT DISTINCT c.nombre,c.nomequipo FROM ciclista c JOIN etapa e ON c.dorsal = e.dorsal;

-- c1 (subconsulta)
  SELECT DISTINCT e.dorsal FROM etapa e;

-- Completa
  SELECT c.nombre,c.nomequipo
    FROM ciclista c 
      JOIN (SELECT DISTINCT e.dorsal FROM etapa e) c1
      USING(dorsal);

/*
  Quiero saber los ciclistas que han ganado puertos y el numero
  de puertos que han ganado. Del ciclista quiere saber el dorsal 
  y el nombre  
  dorsal,nombre,numeroPuertos
*/

  -- C1
  SELECT DISTINCT c.dorsal,c.nombre,p.nompuerto 
  FROM puerto p JOIN ciclista c USING(dorsal);

  -- completa
  SELECT c1.dorsal,c1.nombre,COUNT(*) numeroPuertos 
    FROM (
      SELECT DISTINCT c.dorsal,c.nombre,p.nompuerto 
      FROM puerto p JOIN ciclista c USING(dorsal)
    ) c1
    GROUP BY c1.dorsal,c1.nombre;
