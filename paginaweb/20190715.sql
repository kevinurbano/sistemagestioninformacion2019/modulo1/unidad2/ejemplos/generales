﻿USE paginaweb;

/*
  1.- Titulo del libro y nombre del autor que lo ha escrito.
*/
  -- Producto Cartesiano
  SELECT a.nombre_completo, l.titulo 
    FROM libro l, autor a
    WHERE l.autor=a.id_autor;
  
  -- Con JOIN
  SELECT a.nombre_completo,l.titulo 
    FROM libro l JOIN autor a
    ON l.autor = a.id_autor;

  -- Con JOIN como producto cartesiano (no utilizar)
  SELECT a.nombre_completo,l.titulo 
    FROM libro l JOIN autor a
    WHERE l.autor = a.id_autor;

/*
  2.- Nombre del autor y titulo del libro en 
  el que han ayudado.
*/

SELECT a1.nombre_completo,l.titulo
  FROM ayuda a 
  JOIN libro l ON a.libro = l.id_libro
  JOIN autor a1 ON a.autor = a1.id_autor;